import sys
import images
import transforms
def main():
    try:
        archivo = sys.argv[1]
    except IndexError:
        print("Este archivo no existe o no se ha pasado un nombre de archivo como argumento")
        sys.exit(1)
    try:
        nombre = archivo.split(".jpg")
    except:
        print("El archivo no tiene extension .jpg")
        sys.exit(1)

    funcion = sys.argv[2]
    image = images.read_img(archivo)

    if funcion == "rotate_right":
        imagen = transforms.rotate_right(image)
    elif funcion == "mirror":
        imagen = transforms.mirror(image)
    elif funcion == "blur":
        imagen = transforms.blur(image)
    elif funcion == "greyscale":
        imagen = transforms.greyscale(image)
    elif funcion == "sepia":
        imagen = transforms.sepia(image)
    elif funcion == "aumentar_brillo":
        imagen = transforms.aumentar_brillo(image)
    elif funcion == "disminuir_brillo":
        imagen = transforms.disminuir_brillo(image)
    elif funcion == "aumentar_contraste":
        imagen = transforms.aumentar_contraste(image)
    elif funcion == "disminuir_contraste":
        imagen = transforms.disminuir_contraste(image)
    else:
        print("La funcion introducida no existe")
        sys.exit(1)

    images.write_img(imagen, f"{nombre[0]}_trans.jpg")


if __name__ == '__main__':
    main()
