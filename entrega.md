#ENTREGA CONVOCATORIA ENERO
Mario Yuste Medina (m.yuste.2023@alumnos.urjc.es)
https://youtu.be/yF4_mEx9Ze4?si=UAyBS_NboB6xSaRL
Requisitos Minimos: Método change_colors, Método rotate_right, Método mirror, Método rotate_colors, Método blur, Método shift, Método crop, Método greyscale, Método filter, Programa transform_simple.py, Programa trasnsform_args.py y Programa transform_multi.py
Requisitos Opcionales: Metodo sepia, Metodo aumentar_brillo, Metodo disminuir_brillo, Metodo aumentar_contraste, Metodo disminuir_contraste y un programa interactivo en la forma de una ventana que permite elegir el archivo al que se aplicaran los filtros y el filtro a aplicar, siendo los filtros disponibles Greyscale, Blur, Espejo, Rotar a la Izquierda, Cambio de color y Filter (Esta ventana se abre al ejecutar el codigo ventana.py)
