from PIL import Image
import matplotlib.pyplot as plt

def open_image(filename):
    img = Image.open(filename)
    return img

def show_image(img):
    plt.imshow(img)
    plt.show()