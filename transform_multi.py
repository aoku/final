import sys
import images
import transforms

def main():
    var = 0

    try:
        archivo = sys.argv[1]
        var += 1
    except IndexError:
        print("Este archivo no existe o no se ha pasado un nombre de archivo como argumento")
        sys.exit(1)

    nombre = archivo.split(".jpg")
    numero = nombre.count("")
    if numero < 1:
        print("El archivo no tiene extension .jpg")
        sys.exit(1)
    image = images.read_img(archivo)
    variable = len(sys.argv)-1
    while var < variable:
        funcion = sys.argv.pop(2)
        var += 1

        if funcion == "change_colors":
            try:
                num1 = int(sys.argv.pop(2))
                num2 = int(sys.argv.pop(2))
                num3 = int(sys.argv.pop(2))
                num4 = int(sys.argv.pop(2))
                num5 = int(sys.argv.pop(2))
                num6 = int(sys.argv.pop(2))
            except IndexError:
                print("Los valores introducidos no son correctos o no son numeros enteros")
                sys.exit(1)
            var += 6
            tup1 = (num1, num2, num3)
            tup2 = (num4, num5, num6)
            imagen = transforms.change_colors(image, tup1, tup2)

        elif funcion == "rotate_right":
            imagen = transforms.rotate_right(image)

        elif funcion == "mirror":
            imagen = transforms.mirror(image)

        elif funcion == "rotate_colors":
            try:
                incremento = int(sys.argv.pop(2))
            except IndexError:
                print("No se ha introducido numero para el incremento de colores")
                sys.exit(1)
            var += 1
            imagen = transforms.rotate_colors(image, incremento)

        elif funcion == "blur":
            imagen = transforms.blur(image)

        elif funcion == "filter":
            try:
                num1 = float(sys.argv.pop(2))
                num2 = float(sys.argv.pop(2))
                num3 = float(sys.argv.pop(2))

            except IndexError:
                print("Los valores introducidos no son correctos")
                sys.exit(1)
            var += 3
            imagen = transforms.filter(image, num1, num2, num3)

        elif funcion == "crop":
            try:
                valorx = int(sys.argv.pop(2))
                valory = int(sys.argv.pop(2))
                ancho = int(sys.argv.pop(2))
                alto = int(sys.argv.pop(2))

            except IndexError:
                print("Faltan valores, se debe introducir ancho y alto")
                sys.exit(1)
            var += 4
            imagen = transforms.crop(image, valorx, valory, ancho, alto)

        elif funcion == "shift":
            try:
                horizontal = int(sys.argv.pop(2))
                vertical = int(sys.argv.pop(2))
            except IndexError:
                print("No se han introducido valores enteros")
                sys.exit(1)
            var += 2
            imagen = transforms.shift(image, horizontal, vertical)

        elif funcion == "greyscale":
            imagen = transforms.greyscale(image)

        elif funcion == "sepia":
            imagen = transforms.sepia(image)

        elif funcion == "aumentar_brillo":
            imagen = transforms.aumentar_brillo(image)

        elif funcion == "disminuir_brillo":
            imagen = transforms.disminuir_brillo(image)

        elif funcion == "aumentar_contraste":
            imagen = transforms.aumentar_contraste(image)

        elif funcion == "disminuir_contraste":
            imagen = transforms.disminuir_contraste(image)

        else:
            print("La funcion introducida no existe")
            sys.exit(1)
        image = imagen
        images.write_img(imagen, f"{nombre[0]}_trans.jpg")


if __name__ == '__main__':
    main()
