import sys
import images
import transforms

def main():
    try:
        archivo = sys.argv[1]
    except IndexError:
        print("Este archivo no existe o no se ha pasado un nombre de archivo como argumento")
        sys.exit(1)
    try:
        nombre = archivo.split(".jpg")
    except:
        print("El archivo no tiene extension .jpg")
        sys.exit(1)

    Funcion = sys.argv[2]
    image = images.read_img(archivo)

    if Funcion == "change_colors":
        try:
            num1 = int(sys.argv[3])
            num2 = int(sys.argv[4])
            num3 = int(sys.argv[5])
            num4 = int(sys.argv[6])
            num5 = int(sys.argv[7])
            num6 = int(sys.argv[8])
        except IndexError:
            print("Los valores introducidos no son correctos o no son numeros enteros ")
            sys.exit(1)
        tup1 = (num1, num2, num3)
        tup2 = (num4, num5, num6)
        imagen = transforms.change_colors(image, tup1, tup2)

    elif Funcion == "rotate_right":
        imagen = transforms.rotate_right(image)

    elif Funcion == "mirror":
        imagen = transforms.mirror(image)

    elif Funcion == "rotate_colors":
        try:
            incremento = int(sys.argv[3])
        except IndexError:
            print("No se ha introducido numero para el incremento de colores")
            sys.exit(1)
        imagen = transforms.rotate_colors(image, incremento)

    elif Funcion == "blur":
        imagen = transforms.blur(image)

    elif Funcion == "filter":
        try:
            num1 = float(sys.argv[3])
            num2 = float(sys.argv[4])
            num3 = float(sys.argv[5])

        except IndexError:
            print("Los valores introducidos no son correctos")
            sys.exit(1)
        imagen = transforms.filter(image, num1, num2, num3)

    elif Funcion == "crop":
        try:
            valorx = int(sys.argv[3])
            valory = int(sys.argv[4])
            ancho = int(sys.argv[5])
            alto = int(sys.argv[6])

        except IndexError:
            print("Faltan valores, se debe introducir ancho y alto")
            sys.exit(1)
        imagen = transforms.crop(image, valorx, valory, ancho, alto)

    elif Funcion == "shift":
        try:
            horizontal = int(sys.argv[3])
            vertical = int(sys.argv[4])
        except IndexError:
            print("No se han introducido valores enteros")
            sys.exit(1)
        imagen = transforms.shift(image, horizontal, vertical)

    elif Funcion == "greyscale":
        imagen = transforms.greyscale(image)

    elif Funcion == "sepia":
        imagen = transforms.sepia(image)
    elif Funcion == "aumentar_brillo":
        imagen = transforms.aumentar_brillo(image)
    elif Funcion == "disminuir_brillo":
        imagen = transforms.disminuir_brillo(image)
    elif Funcion == "aumentar_contraste":
        imagen = transforms.aumentar_contraste(image)
    elif Funcion == "disminuir_contraste":
        imagen = transforms.disminuir_contraste(image)

    else:
        print("La funcion introducida no existe")
        sys.exit(1)

    images.write_img(imagen, f"{nombre[0]}_trans.jpg")


if __name__ == '__main__':
    main()
