import tkinter as tk
from tkinter import filedialog
import apertura_imagen
import transforms
import numpy as np

root = tk.Tk()


def seleccionar_archivo():
    filename = filedialog.askopenfilename()
    return filename

def obtener_incremento():
  incremento = tk.simpledialog.askinteger("Rotate Colors", "Enter increment:")
  return incremento

def obtener_valores():
  r = tk.simpledialog.askfloat("Filter", "Valor Rojo:")
  g = tk.simpledialog.askfloat("Filter", "Valor Verde:")
  b = tk.simpledialog.askfloat("Filter", "Valor Azul:")
  return r, g, b



def apply_filter(filename, funcion):
    image = apertura_imagen.open_image(filename)
    image = np.array(image)
    image = image.tolist()





    if funcion == "Grayscale":
        image = transforms.greyscale(image)
    elif funcion == "Blur":
        image = transforms.blur(image)
    elif funcion == "Rotar a la Izquierda":
        image = transforms.rotate_right(image)
    elif funcion == "Espejo":
        image = transforms.mirror(image)
    elif funcion == "Cambio de Colores":
        incremento = obtener_incremento()
        image = transforms.rotate_colors(image, incremento)
    elif funcion == "Filter":
        r, g, b = obtener_valores()
        image = transforms.filter(image, r, g, b)

    image = np.array(image)
    apertura_imagen.show_image(image)




filename = seleccionar_archivo()

opciones_de_filtro = ["Original", "Grayscale", "Blur", "Rotar a la Izquierda", "Espejo", "Cambio de Colores", "Filter"]
filter_var = tk.StringVar(root)
filter_var.set(opciones_de_filtro[0])

filter_dropdown = tk.OptionMenu(root, filter_var, *opciones_de_filtro)
filter_dropdown.pack()

apply_button = tk.Button(root, text="Aplicar Filtro",
                         command=lambda: apply_filter(filename, filter_var.get()))
apply_button.pack()

root.mainloop()
