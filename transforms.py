import images


def change_colors(image: list[list[tuple[int, int, int]]], to_change: tuple[int, int, int], to_change_to: tuple[int, int, int]) -> list[list[tuple[int, int, int]]]:
    imagen = []
    for lista in image:
        var1 = []
        for tupla in lista:
            if tupla == to_change:
                var1.append(to_change_to)
            else:
                var1.append(tupla)
        imagen.append(var1)

    return imagen


def rotate_right(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    width, height = images.size(image)
    pixels = []
    for lista in image:
        for tupla in lista:
            pixels.append(tupla)

    pixels_xy = []
    for x in range(height):
        pixels_xy.append([0] * width)
        for y in range(width):
            pixels_xy[x][y] = pixels[(y * height) + x]
    return pixels_xy


def mirror(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    size = len(image)
    contador = 0
    tamaño = size - 1
    for num in range(size - 1):
        contador += 1
        if contador <= size // 2:
            image[num], image[tamaño] = image[tamaño], image[num]
            tamaño -= 1

    return image


def rotate_colors(image: list[list[tuple[int, int, int]]], increment: int) -> list[list[tuple[int, int, int]]]:
    for lista in image:
        for tupla in lista:
            num_lista = image.index(lista)
            num_tupla = lista.index(tupla)
            tup1 = tupla[0] + increment
            tup2 = tupla[1] + increment
            tup3 = tupla[2] + increment
            if tup1 > 256:
                num = tup1 / 256
                value = tup1 // 256
                numero = num - value
                tup1 = int(256 * numero)
            if tup2 > 256:
                num = tup2 / 256
                value = tup2 // 256
                numero = num - value
                tup2 = int(256 * numero)
            if tup3 > 256:
                num = tup3 / 256
                value = tup3 // 256
                numero = num - value
                tup3 = int(256 * numero)

            tup1 = abs(tup1)
            tup2 = abs(tup2)
            tup3 = abs(tup3)
            image[num_lista][num_tupla] = (tup1, tup2, tup3)

    return image


def blur(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    izquierda = 0
    derecha = 0
    arriba = 0
    abajo = 0

    for lista in image:
        for tupla in lista:
            num_lista = image.index(lista)
            num_tupla = lista.index(tupla)
            if num_lista == 0:
                if num_tupla == 0:
                    derecha = image[num_lista][num_tupla + 1]
                    abajo = image[num_lista + 1][num_tupla]

                elif num_tupla == len(lista) - 1:
                    izquierda = image[num_lista][num_tupla - 1]
                    abajo = image[num_lista + 1][num_tupla]

                else:
                    izquierda = image[num_lista][num_tupla - 1]
                    derecha = image[num_lista][num_tupla + 1]
                    abajo = image[num_lista + 1][num_tupla]

            elif num_lista == len(image) - 1:

                if num_tupla == 0:
                    derecha = image[num_lista][num_tupla + 1]
                    arriba = image[num_lista - 1][num_tupla]

                elif num_tupla == len(lista) - 1:
                    izquierda = image[num_lista][num_tupla - 1]
                    arriba = image[num_lista - 1][num_tupla]

                else:
                    izquierda = image[num_lista][num_tupla - 1]
                    derecha = image[num_lista][num_tupla + 1]
                    arriba = image[num_lista - 1][num_tupla]

            elif num_tupla == 0:
                derecha = image[num_lista][num_tupla + 1]
                arriba = image[num_lista - 1][num_tupla]
                abajo = image[num_lista + 1][num_tupla]

            elif num_tupla == len(lista) - 1:
                izquierda = image[num_lista][num_tupla - 1]
                arriba = image[num_lista - 1][num_tupla]
                abajo = image[num_lista + 1][num_tupla]

            else:
                izquierda = image[num_lista][num_tupla - 1]
                derecha = image[num_lista][num_tupla + 1]
                arriba = image[num_lista - 1][num_tupla]
                abajo = image[num_lista + 1][num_tupla]

            if izquierda == 0:
                if arriba == 0:
                    sum1 = tupla[0] + derecha[0] + abajo[0]
                    sum2 = tupla[1] + derecha[1] + abajo[1]
                    sum3 = tupla[2] + derecha[2] + abajo[2]
                    new_tupla = (sum1 // 3, sum2 // 3, sum3 // 3)
                    image[num_lista][num_tupla] = new_tupla
                elif abajo == 0:
                    sum1 = tupla[0] + derecha[0] + arriba[0]
                    sum2 = tupla[1] + derecha[1] + arriba[1]
                    sum3 = tupla[2] + derecha[2] + arriba[2]
                    new_tupla = (sum1 // 3, sum2 // 3, sum3 // 3)
                    image[num_lista][num_tupla] = new_tupla
                else:
                    sum1 = tupla[0] + derecha[0] + arriba[0] + abajo[0]
                    sum2 = tupla[1] + derecha[1] + arriba[1] + abajo[1]
                    sum3 = tupla[2] + derecha[2] + arriba[2] + abajo[2]
                    new_tupla = (sum1 // 4, sum2 // 4, sum3 // 4)
                    image[num_lista][num_tupla] = new_tupla

            elif derecha == 0:
                if arriba == 0:
                    sum1 = tupla[0] + izquierda[0] + abajo[0]
                    sum2 = tupla[1] + izquierda[1] + abajo[1]
                    sum3 = tupla[2] + izquierda[2] + abajo[2]
                    new_tupla = (sum1 // 3, sum2 // 3, sum3 // 3)
                    image[num_lista][num_tupla] = new_tupla
                elif abajo == 0:
                    sum1 = tupla[0] + izquierda[0] + arriba[0]
                    sum2 = tupla[1] + izquierda[1] + arriba[1]
                    sum3 = tupla[2] + izquierda[2] + arriba[2]
                    new_tupla = (sum1 // 3, sum2 // 3, sum3 // 3)
                    image[num_lista][num_tupla] = new_tupla
                else:
                    sum1 = tupla[0] + izquierda[0] + arriba[0] + abajo[0]
                    sum2 = tupla[1] + izquierda[1] + arriba[1] + abajo[1]
                    sum3 = tupla[2] + izquierda[2] + arriba[2] + abajo[2]
                    new_tupla = (sum1 // 4, sum2 // 4, sum3 // 4)
                    image[num_lista][num_tupla] = new_tupla
            elif arriba == 0:
                sum1 = tupla[0] + izquierda[0] + derecha[0] + abajo[0]
                sum2 = tupla[1] + izquierda[1] + derecha[1] + abajo[0]
                sum3 = tupla[2] + izquierda[2] + derecha[2] + abajo[0]
                new_tupla = (sum1 // 4, sum2 // 4, sum3 // 4)
                image[num_lista][num_tupla] = new_tupla
            elif abajo == 0:
                sum1 = tupla[0] + izquierda[0] + derecha[0] + arriba[0]
                sum2 = tupla[1] + izquierda[1] + derecha[1] + arriba[1]
                sum3 = tupla[2] + izquierda[2] + derecha[2] + arriba[2]
                new_tupla = (sum1 // 4, sum2 // 4, sum3 // 4)
                image[num_lista][num_tupla] = new_tupla
            else:
                sum1 = tupla[0] + izquierda[0] + derecha[0] + arriba[0] + abajo[0]
                sum2 = tupla[1] + izquierda[1] + derecha[1] + arriba[1] + abajo[1]
                sum3 = tupla[2] + izquierda[2] + derecha[2] + arriba[2] + abajo[2]
                new_tupla = (sum1 // 5, sum2 // 5, sum3 // 5)
                image[num_lista][num_tupla] = new_tupla
    return image


def shift(image: list[list[tuple[int, int, int]]], horizontal: int, vertical: int) -> list[list[tuple[int, int, int]]]:
    width, height = images.size(image)
    num = abs(horizontal)
    nums = abs(vertical)
    var2 = images.create_blank(num, height)
    if horizontal >= 0:
        imagen = var2 + image

    else:
        imagen = image + var2

    for lista in imagen:
        for xx in range(nums):
            if vertical >= 0:
                lista.append((0, 0, 0))
            else:
                lista.insert(0, (0, 0, 0))

    return imagen


def filter(image: list[list[tuple[int, int, int]]], r: float, g: float, b: float) -> list[list[tuple[int, int, int]]]:
    imagen = []
    for lista in image:
        var3 = []
        for tupla in lista:
            tup1 = tupla[0] * r
            tup2 = tupla[1] * g
            tup3 = tupla[2] * b
            if tup1 > 255:
                tup1 = 255
            if tup2 > 255:
                tup2 = 255
            if tup3 > 255:
                tup3 = 255

            tupla = (int(tup1), int(tup2), int(tup3))
            var3.append(tupla)
        imagen.append(var3)
    return imagen


def crop(image: list[list[tuple[int, int, int]]], x: int, y: int, width: int, height: int) -> list[list[tuple[int, int, int]]]:
    max_x = x + width
    max_y = y + height
    imagen = []
    for var4 in range(x, max_x):
        lista = []
        for todo in range(y, max_y):
            num = image[var4][todo]
            lista.append(num)

        imagen.append(lista)

    return imagen


def greyscale(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    imagen = []
    for lista in image:
        var5 = []
        for tupla in lista:
            new_val = int((tupla[0] + tupla[1] + tupla[2]) // 3)
            if new_val > 255:
                new_val = 255
            new_tupla = (new_val, new_val, new_val)
            var5.append(new_tupla)

        imagen.append(var5)
    return imagen

def sepia(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
  imagen = []
  for lista in image:
    var6 = []
    for tupla in lista:
      new_r = int(tupla[0] * 0.393 + tupla[1] * 0.769 + tupla[2] * 0.189)
      new_g = int(tupla[0] * 0.349 + tupla[1] * 0.686 + tupla[2] * 0.168)
      new_b = int(tupla[0] * 0.272 + tupla[1] * 0.534 + tupla[2] * 0.131)
      new_r = min(255, new_r)
      new_g = min(255, new_g)
      new_b = min(255, new_b)
      new_tupla = (new_r, new_g, new_b)
      var6.append(new_tupla)

    imagen.append(var6)
  return imagen

def aumentar_brillo(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
  imagen = []
  for lista in image:
    var7 = []
    for tupla in lista:
      new_r = min(255, tupla[0] + 50)
      new_g = min(255, tupla[1] + 50)
      new_b = min(255, tupla[2] + 50)
      new_tupla = (new_r, new_g, new_b)
      var7.append(new_tupla)

    imagen.append(var7)
  return imagen

def disminuir_brillo(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
  imagen = []
  for lista in image:
    var8 = []
    for tupla in lista:
      new_r = max(0, tupla[0] - 50)
      new_g = max(0, tupla[1] - 50)
      new_b = max(0, tupla[2] - 50)
      new_tupla = (new_r, new_g, new_b)
      var8.append(new_tupla)

    imagen.append(var8)
  return imagen

def aumentar_contraste(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
  factor = 1.5
  imagen = []
  for lista in image:
    var9 = []
    for tupla in lista:
      new_r = max(0, min(255, int(factor * (tupla[0] - 128) + 128)))
      new_g = max(0, min(255, int(factor * (tupla[1] - 128) + 128)))
      new_b = max(0, min(255, int(factor * (tupla[2] - 128) + 128)))
      new_tupla = (new_r, new_g, new_b)
      var9.append(new_tupla)
    imagen.append(var9)
  return imagen

def disminuir_contraste(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
  factor = 0.5
  imagen = []
  for lista in image:
    var10 = []
    for tupla in lista:
      new_r = max(0, min(255, int(factor * (tupla[0] - 128) + 128)))
      new_g = max(0, min(255, int(factor * (tupla[1] - 128) + 128)))
      new_b = max(0, min(255, int(factor * (tupla[2] - 128) + 128)))
      new_tupla = (new_r, new_g, new_b)
      var10.append(new_tupla)
    imagen.append(var10)
  return imagen


